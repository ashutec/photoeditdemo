import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { takePicture, requestPermissions, isAvailable } from 'nativescript-camera';
import { PhotoEditor, PhotoEditorControl  } from "nativescript-photo-editor";
import { fromAsset, ImageSource } from 'tns-core-modules/image-source';
import { ImageCropper, OptionsCommon } from "nativescript-imagecropper";
import { ImageAsset } from 'tns-core-modules/image-asset/image-asset';
import { path,knownFolders} from 'tns-core-modules/file-system/file-system';




@Component({
  selector: 'ns-photo-edit',
  templateUrl: './photo-edit.component.html',
  styleUrls: ['./photo-edit.component.css'],
  moduleId: module.id ,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PhotoEditComponent implements OnInit {

  isCameraAvailable: Boolean = false;
  cameraSource: string | ImageAsset = 'res://logo';
  resultSource: string | ImageAsset = 'res://logo';
  
  
  constructor(private cd: ChangeDetectorRef) { }

  async ngOnInit() {
    await requestPermissions();
    this.isCameraAvailable = isAvailable();
    this.cd.detectChanges();
  }

  async onTakePhoto() {
    try {
      
      // Take picture using camera
      this.cameraSource  = await takePicture();

      // Edit photo
      const photoEditor = new PhotoEditor();
      // const result: ImageSource = await photoEditor.editPhoto({
      //   imageSource: await fromAsset(this.cameraSource),
      //   hiddenControls: [PhotoEditorControl.Text,PhotoEditorControl.Draw]
      // }); 

      const imageCropper:ImageCropper = new ImageCropper();

      const result: ImageSource = (await imageCropper.show(await fromAsset(this.cameraSource),{width:100,height:100, lockSquare: false})).image;

      // This is stupid thing to do as when ImageSource is used it does not work
      const filePath = path.join(knownFolders.temp().path,`${(new Date()).getTime()}.jpg`);
      result.saveToFile(filePath,"jpg");

      this.resultSource = new ImageAsset(filePath);      

      this.cd.detectChanges();

    } catch (error) {
      console.error(error);
    }
  }

}
